const Hapi = require("hapi");
const Inert = require("inert");
const log4js = require("log4js");
const fs = require("fs");

const appenders = 
{
	console: { type: "console" }
};
/*{
	console: { type: "console" },
	file: { type: "file", filename: "logs/sot.log", maxLogSize: 10485760, keepFileExt: true }
};*/

log4js.configure
({
	appenders: appenders,
	categories:
	{
		default: { appenders: ["console"], level: "trace" }
	}
});

//load modules
const config = require("./config/AppConfig.json");
const db = require("./modules/Database.js");
const elevenia = require("./modules/EleveniaWrapper.js");
//-----------

const logger = log4js.getLogger();

async function init()
{
	const server = Hapi.server(
	{
		port: 8080,
		host: "localhost",
		routes:
		{
			cors: true
		}
	});

	await server.register(Inert);

	initRouter(server);
	initDatabase();
	initThirdParty();

	await server.start();
	logger.debug("Server running on %s", server.info.uri);
}

function initRouter(server)
{
	server.route(
	{
		method: "GET",
		path: "/",
		handler: function (req, h)
		{
			return "Hello World !!!";
		}
	});

	server.route(
	{
		method: "GET",
		path: "/res/{resName}",
		handler: function(req, h)
		{
			const params = req.params;
			console.log(params);
			return h.file("./public/" + params.resName);
		}
	});

	server.route(
	{
		method: "POST",
		path: "/upload",
		options:
		{
			payload:
			{
				output: "stream"
			}
		},
		handler: function(req, h)
		{
			const payload = req.payload;
			const data = payload.file._data;
			const dataLen = data.length;
			const fileName = payload.file.hapi.filename;

			console.log("[" + dataLen + "] : " + payload);
			const prdimage01 = (dataLen > 0 ? "http://localhost:8080/res/" + fileName : payload.prdimage01);
			if(payload.prdno != null)
			{
				elevenia.updateProduct(
				{
					prdno: payload.prdno,
					prdnm: payload.prdnm,
					htmldetail: payload.htmldetail,
					selprc: payload.selprc,
					prdimage01: prdimage01
				});
			}

			if(dataLen > 0)
			{
				fs.writeFile("./public/" + fileName, data, function(err)
				{
					console.log("err : " + err);
				});
			}

			return h.redirect("http://localhost:3000");
		}
	});

	server.route(
	{
		method: "GET",
		path: "/fetch/{sync?}",
		handler: async function(req, h)
		{
			const params = req.params;
			const sync = params.sync == "true" ? true : false;
			console.log("sync : " + sync);
			
			return await elevenia.fetch(sync);
		}
	});

	server.route(
	{
		method: "POST",
		path: "/update",
		handler: function(req, h)
		{
			var rec = req.payload;
			elevenia.updateProduct(rec);
			return rec;
		}
	});

	server.route(
	{
		method: "POST",
		path: "/delete",
		handler: function(req, h)
		{
			var rec = req.payload;
			elevenia.deleteProduct(rec.prdno);
			return rec;
		}
	});
}

function initDatabase()
{
	db.init(config.DBConfig);

//	db.query("SELECT * FROM t_item where name like $2 or id = $1", [5, "te%"]);
}

function initThirdParty()
{
	elevenia.init(config.Elevenia);
}

process.on("unhandledRejection", function(err)
{
	logger.debug(err);
	process.exit(1);
});

init();
