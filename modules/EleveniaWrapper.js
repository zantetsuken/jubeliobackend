const log4js = require("log4js");
const logger = log4js.getLogger();
const request = require("request");
const xmljs = require("xml2js");
const Entities = require("html-entities").AllHtmlEntities;
const entities = new Entities();

const db = require("./Database.js");
const config = require("../config/AppConfig.json");

function EleveniaWrapper()
{
	var me = this;
	this.Config = 
	{
		ApiKey: ""
	};

	this.init = function(cfg)
	{
		me.doLog("init");
		me.Config = Object.assign({}, cfg);
		logger.trace(me.Config);
	}

	this.fetch = async function(sync)
	{
		return new Promise(function(onSuccess, onFailed)
		{
			var proceed = async function()
			{
				onSuccess(await db.query("SELECT * FROM products ORDER BY prdnm ASC"));
			};

			console.log(sync + " == true : " + (sync == true));
			if(sync == true)
			{
				me.getProductList(1, function()
				{
					proceed();
				});
			}
			else
				proceed();
		});
	}

	this.getProductList = async function(page, onCompleted)
	{
		if(!page) page = 1;
		me.doLog("getProductList[page : " + page + "]");

		const options = 
		{
			method: "GET",
			url: "http://api.elevenia.co.id/rest/prodservices/product/listing?page=" + page,
			headers:
			{
				"openapikey": config.Elevenia.ApiKey
			}
		};

		me.doLog("options : " + JSON.stringify(options));
		request(options, async function(error, response, body)
		{
			if(error != null)
				throw new Error(error);

			var products = null;
			await xmljs.parseString(body, {explicitArray: false}, async function(err, result)
			{
				if(err)
					throw new Error(err);
				
				products = result.Products.product;
			});

			const len = (products == null ? 0 : products.length);
			console.log("Products : " + len);

			if(len > 0)
				await me.insertProducts(products, function()
				{
					onCompleted();
				});
			else
				onCompleted();

//			if(len > 0) 
//				me.getProductList(page + 1, onCompleted);
//			else
//				onCompleted();
		});
	}

	this.getProductDetail = async function(prdNo)
	{
		return new Promise(function(onSuccess, onFailed)
		{
			if(!prdNo) throw new Error("invalid prdNo");
			me.doLog("getProductDetail[prdNo : " + prdNo + "]");

			const options = 
			{
				method: "GET",
				url: "http://api.elevenia.co.id/rest/prodservices/product/details/" + prdNo,
				headers:
				{
					"openapikey": config.Elevenia.ApiKey
				}
			};

			me.doLog("options : " + JSON.stringify(options));
			request(options, async function(error, response, body)
			{
				if(error != null)
					throw new Error(error);

				var product = null;
				await xmljs.parseString(body, {explicitArray: false}, async function(err, result)
				{
					if(err)
						throw new Error(err);
					
					product = result.Product;
				});

				onSuccess(product);
			});
		});
	}

	this.insertProducts = function(products, onCompleted)
	{
		if(products == null || products.length <= 0)
		{
			me.doLogError("products empty !!!");
			return;
		}

		me.doLog("insertProducts : " + products.length);
		const len = products.length;
		var finished = 0;
		for(var i=0; i<len; i++)
		{
			const p = products[i];
			me.getProductDetail(p.prdNo)
			.then(function(pd)
			{
				var {prdNo, prdNm, sellerPrdCd, prdImage01, htmlDetail, selPrc} = pd;
				htmlDetail = entities.encode(htmlDetail);
//				console.log(prdNo + " : " + prdNm + " : " + sellerPrdCd + " : " + prdImage01 + " : " + selPrc);
				db.query("INSERT INTO products " + 
					"(prdNo, prdNm, sellerPrdCd, prdImage01, htmlDetail, selPrc) values ($1, $2, $3, $4, $5, $6) " +
					"ON CONFLICT ON CONSTRAINT products_pkey DO NOTHING",
					[prdNo, prdNm, sellerPrdCd, prdImage01, htmlDetail, selPrc]
				).catch(function(err)
				{
					me.doLogError(err);
				}).finally(function()
				{
					finished ++;
					if(finished >= len)
						onCompleted();
				});
			});
//			me.doLog("insert : " + JSON.stringify(p));
//			db.query("INSERT INTO products (prdNo, prdNm, sellerPrdCd, prdImage01, htmlDetail, selPrc
		}
	}

	this.updateProduct = function(product)
	{
		if(product == null)
		{
			me.doLogError("product empty !!!");
			return;
		}

		product.htmlDetail = entities.encode(product.htmlDetail);
		me.doLog("updateProduct : " + JSON.stringify(product));

		db.query("UPDATE products SET prdNm = $2, sellerPrdCd = $3, prdImage01 = $4, htmlDetail = $5, selPrc = $6 " +
			"WHERE prdNo = $1",
			[product.prdno, product.prdnm, product.sellerprdcd, product.prdimage01, product.htmldetail, product.selprc]);
	}

	this.deleteProduct = function(prdNo)
	{
		me.doLog("deleteProduct : " + prdNo);
		db.query("DELETE FROM products WHERE prdno = $1", [prdNo]);
	}

	this.doLog = function(log)
	{
		logger.debug("[EleveniaWrapper]." + log);
	}

	this.doLogError = function(log)
	{
		logger.fatal("[EleveniaWrapper]." + log);
	}
}

EleveniaWrapper.instance = null;
EleveniaWrapper.getInstance = function()
{
	var instance = EleveniaWrapper.instance;

	logger.debug("[EleveniaWrapper].getInstance : " + instance);
	if(instance == null)
	{
		instance = new EleveniaWrapper();
		EleveniaWrapper.instance = instance;
	}

	return instance;
}

module.exports = EleveniaWrapper.getInstance();
