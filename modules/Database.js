const log4js = require("log4js");
const logger = log4js.getLogger();

const Pool = require("pg").Pool;

function Database()
{
	var me = this;
	this.Config = 
	{
		host: "",
		port: "",
		database: "",
		user: "",
		password: ""
	};
	var db = null;

	this.init = function(cfg)
	{
		me.doLog("init");
		me.Config = Object.assign({}, cfg);
		logger.trace(me.Config);

		db = new Pool(me.Config);

		me.query("CREATE TABLE IF NOT EXISTS products (" +
			"prdNo int not null primary key, " +
			"prdNm varchar(255) not null, " +
			"sellerPrdCd varchar(50), " +
			"prdImage01 varchar(255), " +
			"htmlDetail text, " +
			"selPrc decimal(12,2))", null)
		.then(function(results)
		{
			me.doLog("Init success...");

//			me.query("INSERT INTO products (prdNo, prdNm, sellerPrdCd, prdImage01, htmlDetail, selPrc) " +
//				"values ($1, $2, $3, $4, $5, $6)",
//				[1, "Test Prod", "B0001", "Image.png", "Prod Description Test", 50])
//			me.query("UPDATE products SET selPrc = 50350 WHERE prdNo = 1")
//			.then(function(res)
//			{
//				me.doLog("Insert success : " + res);
//			}).catch(function(err)
//			{
//				me.doLogError("Insert failed : " + err);
//			});

		}).catch(function(err)
		{
			me.doLogError("Init failed !!! : " + err);
		});
	}

	this.query = function(sql, criteria)
	{
		return new Promise(function(onSuccess, onFailed)
		{
			if(db == null)
			{
				//throw new Error("DB not loaded !!!");
				onFailed("DB not loaded !!!");
				return;
			}
			
			me.doLog("query : " + sql + ", criteria : " + JSON.stringify(criteria));
			db.query(sql, criteria)
			.then(function(results)
			{
				me.doLog("query.results : " + results.rows.length);
				onSuccess(results.rows);
			}).catch(function(err)
			{
				me.doLogError("query.err : " + err);
				onFailed(err);
			});
		});
	}

	this.doLog = function(log)
	{
		logger.debug("[Database]." + log);
	}

	this.doLogError = function(log)
	{
		logger.fatal("[Database]." + log);
	}
}

Database.instance = null;
Database.getInstance = function()
{
	var instance = Database.instance;

	logger.debug("[Database].getInstance : " + instance);
	if(instance == null)
	{
		instance = new Database();
		Database.instance = instance;
	}

	return instance;
}

module.exports = Database.getInstance();
